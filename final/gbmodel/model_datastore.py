from .Model import Model
from datetime import datetime
from google.cloud import datastore
from google.cloud import bigquery
import json


def from_datastore(entity):
    if not entity:
        return None
    if isinstance(entity, list):
        entity = entity.pop()
    return [entity['name'],entity['description'],entity['streetAddress'],entity['phoneNumber'],entity['typeService'],entity['hoursOperation'],entity['date'],entity['review']]

def from_realmList(entity):
    if not entity:
        return None
    if isinstance(entity, list):
        entity = entity.pop()
    return [entity['name'],entity['id'], entity['connectedId']]

def from_mountList(entity):
    if not entity:
        return None
    if isinstance(entity, list):
        entity = entity.pop()
    return [entity['id'],entity['name']]

class model(Model):
    def __init__(self):
        self.client = datastore.Client('cloud-f20-shannonmeyer-shmeyer')

#SELECT
    def select(self):
        query = self.client.query(kind = 'Review')
        entities = list(map(from_datastore,query.fetch()))
        return entities

    def selectRealmList(self):
        query = self.client.query(kind = 'RealmList')
        entities = list(map(from_realmList,query.fetch()))
        return entities
    
    def selectMountList(self):
        query = self.client.query(kind = 'MountList')
        entities = list(map(from_mountList,query.fetch()))
        return entities

#INSERT

    def insertMounts(self, name, id):
        key = self.client.key('MountList')
        rev = datastore.Entity(key)
        rev.update( {
            'id': id, 
            'name':name
            })
        self.client.put(rev)
        return True



    def insertRealms(self, name, id, connectedId):
        key = self.client.key('RealmList')
        rev = datastore.Entity(key)
        rev.update( {
            'name':name, 
            'id': id,
            'connectedId' : connectedId
            })
        self.client.put(rev)
        return True

    

    def insert(self,name,description,streetAddress, phoneNumber,typeService, hoursOperation,review ):
        key = self.client.key('Review')
        rev = datastore.Entity(key)
        rev.update( {
            'name':name, 
            'description': description,
            'streetAddress': streetAddress,
            'phoneNumber': phoneNumber,
            'typeService': typeService, 
            'hoursOperation': hoursOperation, 
            'date': datetime.today(), 
            'review':review
            })
        self.client.put(rev)
        return True

#DELETE #TODO
    def deleteMounts(self, id):
        query = self.client.query(kind = 'MountList')
        entities = list(map(from_mountList,query.fetch()))
        return true




#BIG QUERY 
    def insertAH(self, entry):
# Construct a BigQuery client object.
        client = bigquery.Client()

        # TODO(developer): Set table_id to the ID of table to append to.
        table_id = "cloud-f20-shannonmeyer-shmeyer.AuctionHouse.currentAH" 
       
        # SCHEMA
        #1  connected_realm	    INTEGER	REQUIRED	
        #2  auction_id	        INTEGER	REPEATED	
        #3  item_id	            INTEGER	REPEATED	
        #4  bid	                INTEGER	NULLABLE	
        #5  buyout	            INTEGER	NULLABLE	
        #6  unit_price	        INTEGER	NULLABLE	
        #7  quantity	        INTEGER	NULLABLE	
        #8  date	            DATETIME	REQUIRED	

         
        date = str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


        with open("output.json", "w") as outfile: 
            for x in entry:
                connected_realm = (entry[0]['connected_realm']['href'])
                for y in  x['auctions']:
                    a = (str(connected_realm))
                    b = (str(y['id']))
                    c = (str(y['item']['id']))
                    d = '0'
                    e = '0'
                    f = '0'

                    if 'bid' in y.keys():
                        d =  (str(y['bid']))

                    if 'buyout' in y.keys(): 
                        e =   (str(y['buyout']))

                    if 'unit_price' in y.keys(): 
                        f =  (str(y['unit_price']))
                    
                    g = (str(y['quantity']))

                    finalString =  "{u\"connected_realm\":"+a + ",u\"auction_id\":"+b + ",u\"item_id\":"+c + ",u\"bid\":"+d + ",u\"buyout\":"+e+ ",u\"unit_price\":"+f + ",u\"quantity\":"+g + ",u\"date\":\""+date +"\""
                    rows_to_insert = [{u"connected_realm": a  , u"auction_id": b,  u"item_id":c  ,  u"bid": d,  u"buyout": e,  u"unit_price": f, u"quantity": g , u"date": date  } ]
                    errors = client.insert_rows_json(table_id, rows_to_insert)  # Make an API request.
                    if errors == []:
                        print("New rows have been added.")
                    else:
                        print("Encountered errors while inserting rows: {}".format(errors))
                    outfile.write(finalString)
                    outfile.write('},\n')
                   
                

    
        