from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import gbmodel
from blizzardapi import BlizzardApi
from os import environ
import urllib.request


SECRET_KEY = environ.get('SECRET_KEY')
API_KEY = environ.get('API_KEY')

api_client = BlizzardApi(API_KEY, SECRET_KEY)


class Sign(MethodView):
    def get(self):
        
        return render_template('sign.html')

    def post(self):
   
        model = gbmodel.get_model()


        itemID = request.form['id']
                     
        creature_media = api_client.wow.game_data.get_item("us", "en_US", int(itemID),  is_classic=False)

        #creature_media2 = api_client.wow.game_data.get_item_media("us", "en_US", int(itemID),  is_classic=False)




        model.insertMounts(creature_media['name'] , creature_media['id'])


        return redirect(url_for('index'))



