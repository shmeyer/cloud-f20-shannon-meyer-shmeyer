"""
A HW flask app.
"""
import flask
from flask.views import MethodView
from index import Index
from sign import Sign
from charitylist import Charitylist
from auction import Auction

app = flask.Flask(__name__)       # our Flask app


# Routing Page 

app.add_url_rule('/',
                 view_func=Index.as_view('index'),
                 methods=["GET"])

app.add_url_rule('/sign/',
                 view_func=Sign.as_view('sign'),
                 methods=['GET', 'POST'])

app.add_url_rule('/charitylist/',
                 view_func=Charitylist.as_view('charitylist'),
                 methods=['GET'])  

app.add_url_rule('/auction/',
                 view_func=Auction.as_view('auction'),
                 methods=['GET', 'POST'])              

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
