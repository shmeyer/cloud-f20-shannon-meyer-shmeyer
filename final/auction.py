from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import gbmodel
from os import environ
from blizzardapi import BlizzardApi
import json
import time

SECRET_KEY = environ.get('SECRET_KEY')
API_KEY = environ.get('API_KEY')
api_client = BlizzardApi(API_KEY, SECRET_KEY)

# https://github.com/trevorphillipscoding/python-blizzardapi/tree/af5fb714da7e51b67ea7f1de11dc522152895d6f


#sort the incoming dic for the dropdown list
def sortByName(e):
  return e['name']
def sortByConnectedID(e):
  return e['connectedId']


#dump the realm AH data into local file
def printJSON(i,realmAuction):
    r = json.dumps(realmAuction)
    r = json.dumps(realmAuction, separators=(',', ':')) 
    f = open("assests/DUMP" + str(i) + ".json","w")
    f.write(r)
    f.close()
    return

#Used to remove the connected realm information since 
#it is located in a string of a link
# Would require multiple calls to api otherwise
def removeNonNumbers(e):
    nonLetterString = ''.join(filter(lambda i: i.isdigit(), e))
    return nonLetterString



class Auction(MethodView):
    def get(self):
        realmList = api_client.wow.game_data.get_realms_index("us", "en_US", is_classic=False)
        model = gbmodel.get_model()  
        entries = [dict(name=row[0], id=row[1], connectedId=row[2]) for row in model.selectRealmList()] 
        entries.sort(key=sortByName)
        return render_template('auction.html', entries=entries)

    def post(self):
        #get input from user
        realmName = request.form['realmName']

        #get Realm list from data store
        model = gbmodel.get_model()  
        entries = [dict(name=row[0], id=row[1], connectedId=row[2]) for row in model.selectRealmList()] 
        entries.sort(key=sortByName)

        #get connectedId  from datastore list to compare with api request
        connectedId = []
        for x in entries:
            y = x['connectedId']
            if y not in connectedId: 
                connectedId.append(y)
    

        #TODO fix order change html 
        rares = [dict(name=row[0], id=row[1]) for row in model.selectMountList()] 

     
        data_list= []

        #Every Realm Seach
        if realmName == '':
            i = 0
            #loop through every connected realm (less than the total amount of realms)
            #and use the i counter to index the data list to send to the html page
            for x in connectedId:
                realmAuction = api_client.wow.game_data.get_auctions("us", "en_US", x)
                #printJSON(int(x['connectedId']),realmAuction)
                time.sleep(.1) #Limit Timeout From API
                data_list.append (realmAuction)
                data_list[i]['connected_realm']['href'] = removeNonNumbers(data_list[i]['connected_realm']['href'])
                i = i + 1



        #Single Realm Search
        else:
            realmAuction = api_client.wow.game_data.get_auctions("us", "en_US", realmName)
            printJSON(int(realmName),realmAuction)
            data_list.append (realmAuction)
            data_list[0]['connected_realm']['href'] = removeNonNumbers(data_list[0]['connected_realm']['href'])
            print (data_list[0]['connected_realm']['href'])
            
    
        
        return render_template('auction.html', entries=entries, data_list=data_list, rareItems= rares)





      