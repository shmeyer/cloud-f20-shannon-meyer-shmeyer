from flask import render_template
from flask.views import MethodView
import gbmodel
        
"""
Takes the list of entries and prints out to page
"""
class Charitylist(MethodView):
    def get(self):
        model = gbmodel.get_model()
        entries = [dict(id=row[0], name=row[1]) for row in model.selectMountList()]
        return render_template('charitylist.html', entries=entries)
