from flask import render_template
from flask.views import MethodView
import gbmodel
from PIL import Image
from io import BytesIO
import base64
import urllib.request
import json

from os import environ
from blizzardapi import BlizzardApi


SECRET_KEY = environ.get('SECRET_KEY')
API_KEY = environ.get('API_KEY')

api_client = BlizzardApi(API_KEY, SECRET_KEY)

def get_digits(text):
    return filter(str.isdigit, text)

def removeNonNumbers(e):
    nonLetterString = ''.join(filter(lambda i: i.isdigit(), e))
    return nonLetterString

def updateRealmDatastore():    
    realmList = api_client.wow.game_data.get_connected_realms_index("us", "en_US",  is_classic=False)
    res = []
    for x in realmList['connected_realms']:
        res.append (''.join(filter(lambda i: i.isdigit(), x['href'])))
    print(res)

    realmList2 = []
    #
    model = gbmodel.get_model()

    for x in res:
        realmList2 = api_client.wow.game_data.get_connected_realm("us", "en_US", int(x) , is_classic=False)        
        for y in realmList2['realms']:
            print(y['name'] , y['id'], x)
            model.insertRealms( y['name'], y['id'], x)



class Index(MethodView):
    def get(self):
        creature_media2 = api_client.wow.game_data.get_creature_display_media("us", "en_US",30223,  is_classic=False)
        return render_template('index.html', cm2 = creature_media2['assets'][0]['value'])



