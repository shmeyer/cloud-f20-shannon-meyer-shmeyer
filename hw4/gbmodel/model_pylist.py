"""
Python list model
"""
from datetime import date
from .Model import Model

class model(Model):
    def __init__(self):
        self.charity = []

    def select(self):
        """
        Returns guestentries list of lists

        ##HW2 Addition

        Each list in charity 
        | Name   | Description | signed_on  | Street Address |Type of Service   | Phone Number| Hours of Operation
        :return: List of lists
        """
        return self.charity

    def insert(self, name, description, streetAddress, phoneNumber, typeService, hoursOperation, review):
        """
        Appends a new list of values representing new review into guestentries
        :param name: String
         ##HW2 Addition
        :param description: String
        :param phoneNumber: Integer
        :param typeService: String
        :param hoursOperation: String
        :param review: String
        :param Street Address : String
        :return: True
        """
        params = [name, description, streetAddress, phoneNumber, typeService, hoursOperation, date.today(), review]
        self.charity.append(params)
        return True
