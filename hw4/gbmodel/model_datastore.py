from .Model import Model
from datetime import datetime
from google.cloud import datastore

def from_datastore(entity):
    if not entity:
        return None
    if isinstance(entity, list):
        entity = entity.pop()
    return [entity['name'],entity['description'],entity['streetAddress'],entity['phoneNumber'],entity['typeService'],entity['hoursOperation'],entity['date'],entity['review']]


class model(Model):
    def __init__(self):
        self.client = datastore.Client('cloud-f20-shannonmeyer-shmeyer')

    def select(self):
        query = self.client.query(kind = 'Review')
        entities = list(map(from_datastore,query.fetch()))
        return entities

    def insert(self,name,description,streetAddress, phoneNumber,typeService, hoursOperation,review ):
        key = self.client.key('Review')
        rev = datastore.Entity(key)
        rev.update( {
            'name':name, 
            'description': description,
            'streetAddress': streetAddress,
            'phoneNumber': phoneNumber,
            'typeService': typeService, 
            'hoursOperation': hoursOperation, 
            'date': datetime.today(), 
            'review':review
            })
        self.client.put(rev)
        return True
