from flask import render_template
from flask.views import MethodView
import gbmodel
        
"""
Takes the list of entries and prints out to page
"""
class Charitylist(MethodView):
    def get(self):
        model = gbmodel.get_model()
        entries = [dict(name=row[0], description=row[1], streetAddress=row[2], phoneNumber=row[3], typeService=row[4], hoursOperation=row[5],signed_on=row[6], review=row[7]) for row in model.select()]
        return render_template('charitylist.html', entries=entries)
