class Model():
    def select(self):
        """
        Gets all entries from the database
        :return: Tuple containing all rows of database
        """
        pass
    '''
     HW2 Additions
    '''
    def insert(self, name, description, streetAddress, phoneNumber, typeService, hoursOperation, review):
        """
        Inserts entry into database
        :param name: String
        :param description: String
        :param phoneNumber: Integer
        :param typeService: String
        :param hoursOperation: String
        :param streetAddress: String
        :param review: String
        :return: none
        :raises: Database errors on connection and insertion
        """
        pass
