"""
A simple charity flask app.
ata is stored in a SQLite database that looks something like the following:
  
+------------+------------------+------------+----------------+------------------+-------------+
| Name       | Description      | signed_on  | Street Address |Type of Service   | Phone Number| Hours of Operation
+============+==================+============+================+============+===================+
| John Doe   | short paragraph  | 2012-05-28 | 200 SW Park AVE|houseless services| 5555555555  | Hours of Operation
+------------+------------------+------------+----------------+------------+-------------------+

This can be created with the following SQL (see bottom of this file):

    create table charity (name text, description text, signed_on date, review);

"""

"""
    HW2 ADDITIONS:
        Change to charity and change additional schema entries 
"""


from datetime import date
from .Model import Model
import sqlite3
DB_FILE = 'entries.db'    # file for our Database

class model(Model):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from charity")
        except sqlite3.OperationalError:
            cursor.execute("create table charity (name text, description text, streetAddress text, phoneNumber integer, typeService text, hoursOperation text, signed_on date, review)")
        cursor.close()

    def select(self):
        """
        Gets all rows from the database
        Each row contains: name, description, date, review
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM charity")
        return cursor.fetchall()

    def insert(self, name, description, streetAddress, phoneNumber, typeService, hoursOperation, review):
        """
        Inserts entry into database
        :param name: String
        :param description: String
        :param phoneNumber: Integer
        :param streetAddress: String
        :param review: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'name':name, 'description':description, 'streetAddress':streetAddress,'phoneNumber':phoneNumber,'typeService':typeService, 'hoursOperation':hoursOperation, 'date':date.today(), 'review':review}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into charity (name, description, streetAddress, phoneNumber, typeService, hoursOperation, signed_on, review) VALUES (:name, :description, :streetAddress, :phoneNumber, :typeService, :hoursOperation, :date, :review)", params)

        connection.commit()
        cursor.close()
        return True
