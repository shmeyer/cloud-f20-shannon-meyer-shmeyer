from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import gbmodel

class Sign(MethodView):
    def get(self):
        return render_template('sign.html')

    def post(self):
        """
        Accepts POST requests, and processes the form;
        Redirect to index when completed.

        HW2 additions:
            Added to the model with posting to the DB
            Inserts form input to list 
        """
        model = gbmodel.get_model()
        model.insert(request.form['name'], request.form['description'], request.form['streetAddress'], request.form['phoneNumber'], request.form['typeService'], request.form['hoursOperation'], request.form['review'])
        return redirect(url_for('index'))
