from flask import render_template
from flask.views import MethodView
import gbmodel

class Index(MethodView):
    def get(self):
        model = gbmodel.get_model()
        entries = [dict(name=row[0], description=row[1], streetAddress=row[2], phoneNumber=row[3], typeService=row[4], hoursOperation=row[5],signed_on=row[6], message=row[7]) for row in model.select()]
        return render_template('index.html',entries=entries)
